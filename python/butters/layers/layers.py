"""Some layers.
"""
import tensorflow as tf


class ZeroPad2d(tf.layers.Layer):
    def __init__(self,
                 pads=0,
                 data_format="channels_first",
                 name="zeropad2d",
                 **kwargs):
        super(ZeroPad2d, self).__init__(name=name, **kwargs)
        self.pads = pads
        self.data_format = data_format
        pad = [pads, pads]
        nopad = [0, 0]
        if data_format == "channels_first":
            self.paddings = [nopad, nopad, pad, pad]
        else:  # "channels_last"
            self.paddings = [nopad, pad, pad, nopad]
        self.input_spec = tf.layers.InputSpec(ndim=4)

    def call(self, inputs):
        outputs = tf.pad(inputs, self.paddings)
        return outputs

    def compute_output_shape(self, input_shape):
        input_shape = tf.TensorShape(input_shape).as_list()
        if self.data_format == "channels_first":
            return tf.TensorShape([
                input_shape[0],
                input_shape[1],
                input_shape[2] + self.pads,
                input_shape[3] + self.pads])
        else:  # "channels_last"
            return tf.TensorShape([
                input_shape[0],
                input_shape[1] + self.pads,
                input_shape[2] + self.pads,
                input_shape[3]])


def zero_pad2d(inputs,
               pads=0,
               data_format="channels_first",
               name="zeropad2d"):
    """Zero padding layer for 2D inputs.

    Arguments:
        inputs: A rank 4 input tensor.
        pads: Number of 0s to pad on 4 edges.
        data_format: Be either "channels_first" or "channels_last".
        name: Name of the tensor.

    Returns:
        Output tensor.
    """
    layer = ZeroPad2d(pads, data_format, name)
    return layer.apply(inputs)


class LRNLayer(tf.layers.Layer):
    def __init__(self,
                 depth_radius=5,
                 bias=1,
                 alpha=1,
                 beta=0.5,
                 name="lrn",
                 **kwargs):
        super(LRNLayer, self).__init__(name=name, **kwargs)
        self.depth_radius = depth_radius
        self.bias = bias
        self.alpha = alpha
        self.beta = beta

    def call(self, inputs):
        outputs = tf.nn.local_response_normalization(
            inputs,
            self.depth_radius,
            self.bias,
            self.alpha,
            self.beta)
        return outputs

    def compute_output_shape(self, input_shape):
        return input_shape


def lrn(inputs,
        depth_radius=5,
        bias=1,
        alpha=1,
        beta=0.5,
        name="lrn"):
    """Local response normalization.

    See tf.nn.local_response_normalization.
    """
    layer = LRNLayer(depth_radius, bias, alpha, beta, name)
    return layer.apply(inputs)


class WeightedAvgViewPoolLayer(tf.layers.Layer):
    def __init__(self,
                 weight_initials,
                 trainable=True,
                 name=None,
                 **kwargs):
        super(WeightedAvgViewPoolLayer, self).__init__(
            trainable=trainable, name=name, **kwargs)
        self.weight_initials = weight_initials
        self.weight_sum = 1.0

    def build(self, input_shape):
        n_views = len(input_shape)
        if self.weight_initials and \
                len(self.weight_initials) != n_views:
            raise ValueError("The length of weight initials "
                             "must equal to n_views.")

        self.view_weights = []
        for v in range(n_views):
            name = "w" + str(v)
            if self.weight_initials:
                initial = self.weight_initials[v]
            else:
                initial = 1.0 / n_views
            weight = self.add_variable(
                name=name,
                shape=[],
                dtype=tf.float32,
                initializer=tf.constant_initializer(value=initial,
                                                    dtype=tf.float32),
                trainable=True)
            self.view_weights.append(weight)

        self.input_spec = [tf.layers.InputSpec(ndim=4)] * n_views

        self.built = True

    def call(self, inputs):
        views = tf.expand_dims(inputs[0] * self.view_weights[0], 0)
        for view, weight in zip(inputs[1:], self.view_weights[1:]):
            v = tf.expand_dims(view * weight, 0)
            views = tf.concat([views, v], 0)
        outputs = tf.reduce_mean(views, axis=0)
        return outputs

    def compute_output_shape(self, input_shape):
        shape = tf.TensorShape(input_shape).as_list()
        return tf.TensorShape(shape[1:])


def weighted_avg_view_pool(inputs,
                           weight_initials=None,
                           trainable=True,
                           reuse=False,
                           name=None):
    """Weighted average view pooling.

    Arguments:
        inputs: A [V, N, x, x, x] shape tensor.
        weight_initials: Initial weights for views.
        trainable: A boolean.
        reuse: A boolean.
        name: A boolean.

    Returns:
        Output tensor.
    """
    layer = WeightedAvgViewPoolLayer(weight_initials,
                                     name,
                                     trainable,
                                     _reuse=reuse,
                                     _scope=name)
    return layer.apply(inputs)
