"""VGG-M network.
"""
import tensorflow as tf
from .. import layers


def vgg_m_1024(inputs,
               n_classes,
               is_train=True,
               reuse=False,
               data_format="NCHW",
               name="vgg_m_1024"):
    """VGG-M-1024 network.
    """
    if format == "NCHW":
        data_format = "channels_first"
    else:  # NHWC
        data_format = "channels_last"

    with tf.variable_scope(name, reuse=reuse):
        network = tf.layers.conv2d(
            inputs,
            filters=96,
            kernel_size=(7, 7),
            strides=(2, 2),
            padding="VALID",
            data_format=data_format,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="conv1")
        network = layers.lrn(
            network,
            depth_radius=5,
            bias=2,
            alpha=0.0005,
            beta=0.75,
            name="lrn1")
        network = tf.layers.max_pooling2d(
            network,
            pool_size=3,
            strides=2,
            padding="SAME",
            data_format=data_format,
            name="pool1")
        network = tf.layers.conv2d(
            network,
            filters=256,
            kernel_size=(5, 5),
            strides=(2, 2),
            padding="VALID",
            data_format=data_format,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="conv2")
        network = layers.lrn(
            network,
            depth_radius=5,
            bias=2,
            alpha=0.0005,
            beta=0.75,
            name="lrn2")
        network = tf.layers.max_pooling2d(
            network,
            pool_size=3,
            strides=2,
            padding="SAME",
            data_format=data_format,
            name="pool2")
        network = tf.layers.conv2d(
            network,
            filters=512,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="SAME",
            data_format=data_format,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="conv3")
        network = tf.layers.conv2d(
            network,
            filters=512,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="SAME",
            data_format=data_format,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="conv4")
        network = tf.layers.conv2d(
            network,
            filters=512,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="SAME",
            data_format=data_format,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="conv5")
        network = tf.layers.max_pooling2d(
            network,
            pool_size=3,
            strides=2,
            padding="VALID",
            data_format=data_format,
            name="pool5")
        network = tf.layers.flatten(network, name="flatten5")
        network = tf.layers.dense(
            network,
            4096,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="fc6")
        network = tf.layers.dropout(
            network,
            rate=0.5,
            training=is_train,
            name="drop6")
        network = tf.layers.dense(
            network,
            1024,
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="fc7")
        network = tf.layers.dropout(
            network,
            rate=0.5,
            training=is_train,
            name="drop7")
        network = tf.layers.dense(
            network,
            n_classes,
            activation=tf.identity,
            kernel_initializer=tf.contrib.layers.xavier_initializer(),
            trainable=is_train,
            reuse=reuse,
            name="fc8")

    return network
