"""Convert raw images to tfrecords.
"""
import os
import sys
import random
import re
import json
import numpy as np
from PIL import Image
import tensorflow as tf

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string(
    "data_dir", None,
    """Path to raw images.""")
tf.app.flags.DEFINE_string(
    "out_dir", None,
    """Path to output multiview images.""")
tf.app.flags.DEFINE_integer(
    "n_views", 1,
    """Number of views per object.""")
tf.app.flags.DEFINE_string(
    "data_format", "NCHW",
    """The byte format, either NCHW or NHWC.""")
tf.app.flags.DEFINE_boolean(
    "shuffle", True,
    """Whether to shuffle dataset.""")


def _atoi(text):
    return int(text) if text.isdigit() else text


def _natural_keys(text):
    return [_atoi(c) for c in re.split(r'(\d+)', text)]


def _serialize(writer, data_set, data_format):
    for i, elem in enumerate(data_set):
        sys.stdout.write(
            "Serializing {}/{}\r".format(i + 1, len(data_set)))
        label = elem[0]
        files = elem[1]
        file_names = [bytes(f, "utf-8") for f in files]
        imgs = []
        for f in files:
            img = Image.open(f)
            if data_format == "NCHW":
                img_arr = np.array(img)
                img_arr = img_arr.transpose(2, 0, 1)
                imgs.append(img_arr.tobytes())
            else:
                imgs.append(img.tobytes())

        example1 = tf.train.Example(features=tf.train.Features(feature={
            "label": tf.train.Feature(
                int64_list=tf.train.Int64List(value=[label])),
            "file_names": tf.train.Feature(
                bytes_list=tf.train.BytesList(value=file_names)),
            "views": tf.train.Feature(
                bytes_list=tf.train.BytesList(value=imgs))
        }))
        writer.write(example1.SerializeToString())
    sys.stdout.write("\n")

    w, h = img.size
    if data_format == "NCHW":
        shape = [3, w, h]
    else:
        shape = [w, h, 3]
    return shape


def convert_modelnet(data_dir,
                     out_dir,
                     n_views,
                     data_format,
                     shuffle):
    """Convert the images contained in data_dir into TFRecord format.

    Outputs the following files in directory out_dir

    - train.tfrecords
    - validation.tfrecords
    - test.tfrecords
    - meta.json

    Take 10% from the train set as validation set.

    The layout of data_dir should be,

    - data_dir
        - class1
            - train
                - {class}_{number}_{view}.png
            - test
        - class2
            - train
            - test
        - ...

    # Arguments:

    - data_dir: A str for data directory.
    - out_dir: A str for output directory.
    - n_views: Number of views per model.
    - data_format: The image byte format to store, be either NCHW or NHWC.
    - shuffle: Whether to randomly shuffle the image.
    """
    print("Converting images to tfrecords.")
    print("Input directory: ", data_dir)
    print("Output directory: ", out_dir)
    print("Number of views per model: ", n_views)
    print("Image format: ", data_format)
    print("Random shuffle enabled: ", shuffle)
    if not os.access(out_dir, os.F_OK):
        os.mkdir(out_dir)

    train_set = []  # file names
    validation_set = []
    test_set = []
    n_categories = len(os.listdir(data_dir))
    n_total_train = 0
    n_total_val = 0
    n_total_test = 0
    n_cat_train = []  # sizes per category
    n_cat_val = []
    n_cat_test = []

    for label, category in enumerate(sorted(os.listdir(data_dir))):
        train_dir = os.path.join(data_dir, category, "train")
        test_dir = os.path.join(data_dir, category, "test")
        train_files = sorted(os.listdir(train_dir), key=_natural_keys)
        test_files = sorted(os.listdir(test_dir), key=_natural_keys)
        assert len(train_files) % n_views == 0
        assert len(test_files) % n_views == 0

        # n_train = len(train_files) // n_views // 3
        n_train = len(train_files) // n_views
        n_val = n_train // 10
        n_train = (n_train - n_val)
        n_test = len(test_files) // n_views
        n_cat_train.append(n_train)
        n_cat_val.append(n_val)
        n_cat_test.append(n_test)
        n_images = n_train + n_val + n_test
        n_total_train += n_train
        n_total_val += n_val
        n_total_test += n_test
        print("{}({}/{})  train: {}  val: {}  test: {}  total: {}".format(
            category,
            label,
            n_categories - 1,
            n_train,
            n_val,
            n_test,
            n_images))

        train_cnt = 0
        # for j in range(1, len(train_files), 3):
        for j in range(0, len(train_files), n_views):
            views = []
            for k in range(n_views):
                views.append(os.path.join(train_dir, train_files[j + k]))
            if train_cnt < n_train:
                train_set.append((label, views))
                train_cnt += 1
            else:
                validation_set.append((label, views))

        for j in range(0, len(test_files), n_views):
            views = []
            for k in range(n_views):
                views.append(os.path.join(test_dir, test_files[j + k]))
            test_set.append((label, views))
    assert len(train_set) == n_total_train
    assert len(validation_set) == n_total_val
    assert len(test_set) == n_total_test
    print("ALL  train: {}  val: {}  test: {}  total:  {}".format(
        n_total_train,
        n_total_val,
        n_total_test,
        n_total_train + n_total_val + n_total_test))

    if shuffle:
        random.shuffle(train_set)
        random.shuffle(validation_set)
        random.shuffle(test_set)

    # Write tfrecords
    train_writer = tf.python_io.TFRecordWriter(
        os.path.join(out_dir, "train.tfrecords"))
    val_writer = tf.python_io.TFRecordWriter(
        os.path.join(out_dir, "validation.tfrecords"))
    test_writer = tf.python_io.TFRecordWriter(
        os.path.join(out_dir, "test.tfrecords"))

    _serialize(train_writer, train_set, data_format)
    _serialize(val_writer, validation_set, data_format)
    shape = _serialize(test_writer, test_set, data_format)

    # Write label file
    with open(os.path.join(out_dir, "meta.json"), "w") as meta_file:
        cats = []
        for label, category in enumerate(sorted(os.listdir(data_dir))):
            cat = {
                "label": label,
                "name": category,
                "size": [
                    n_cat_train[label],
                    n_cat_val[label],
                    n_cat_test[label]
                ]
            }
            cats.append(cat)
        meta = {
            "source": data_dir,
            "n_categories": n_categories,
            "n_views": n_views,
            "data_format": data_format,
            "shape": shape,
            "size": [n_total_train, n_total_val, n_total_test],
            "categories": cats
        }
        json.dump(meta, meta_file, indent=4)


def _main(_):
    """Main entry.
    """
    convert_modelnet(FLAGS.data_dir,
                     FLAGS.out_dir,
                     FLAGS.n_views,
                     FLAGS.data_format,
                     FLAGS.shuffle)


def main(argv):
    """Dummy main entry.
    """
    tf.app.flags.mark_flags_as_required(["data_dir", "out_dir"])
    tf.app.run(_main, argv)


if __name__ == "__main__":
    main(sys.argv)
