"""Script for training.
"""
import datetime
import json
import logging
import os
import sys
import time
import numpy as np
import tensorflow as tf
from common import model_selector, parse_example

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("data_dir", None,
                           """Directory to tfrecords.""")
tf.app.flags.DEFINE_string("network", "vgg_m_1024",
                           """Choose from ["vgg_m_1024, amvcnn_vgg_m_1024"]""")
tf.app.flags.DEFINE_integer("batch_size", 16,
                            """Batch size.""")
tf.app.flags.DEFINE_integer("train_steps", 2000,
                            """Training steps.""")
tf.app.flags.DEFINE_float("learning_rate", 0.001,
                          """Learning rate.""")
tf.app.flags.DEFINE_integer("decay_steps", 1000,
                            """Steps to decay learning rate.""")
tf.app.flags.DEFINE_float("decay_rate", 0.9,
                          """Decay rate.""")
tf.app.flags.DEFINE_integer("log_steps", 10,
                            """Steps to log training stats.""")
tf.app.flags.DEFINE_string("log_dir", "./log/",
                           """Directory to save log and training summary.""")
tf.app.flags.DEFINE_integer("val_steps", 500,
                            """Steps to run validation.""")
tf.app.flags.DEFINE_float("val_ratio", 0.1,
                          """Ratio of validation set to use.""")
tf.app.flags.DEFINE_integer("save_steps", 1000,
                            """Steps to save the network.""")
tf.app.flags.DEFINE_string("save_dir", "../snapshots/",
                           """Directory to save trained network.""")
tf.app.flags.DEFINE_string("finetune", None,
                           """Path to the model to finetune.""")
tf.app.flags.DEFINE_string("resume", None,
                           """Path to the model to resume training.""")
tf.app.flags.DEFINE_boolean("debug", False,
                            """Whether to turn on debug outputs.""")


def _main(_):
    """Main entry.
    """
    # Resuming should shadow finetuning
    if(FLAGS.finetune and FLAGS.resume):
        FLAGS.finetune = None

    # Logging setup
    if not os.access(FLAGS.log_dir, os.F_OK):
        os.mkdir(FLAGS.log_dir)
    log_level = logging.INFO
    log_format = "%(message)s"
    log_dir = "train_" + datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    log_dir = os.path.join(FLAGS.log_dir, log_dir)
    os.mkdir(log_dir)
    log_file = os.path.join(log_dir, "train.log")
    handlers = [logging.FileHandler(filename=log_file, mode="w"),
                logging.StreamHandler()]
    logging.basicConfig(level=log_level, format=log_format, handlers=handlers)

    # Meta data
    meta_file = open(os.path.join(FLAGS.data_dir, "meta.json"))
    meta = json.load(meta_file)

    logging.info("==================Overview====================")
    logging.info("Dataset directory: %s", FLAGS.data_dir)
    logging.info("Number of categories: %d", meta["n_categories"])
    logging.info("Number of views: %d", meta["n_views"])
    logging.info("Network architecture: %s", FLAGS.network)
    if FLAGS.finetune is not None:
        logging.info("Finetuning from: %s", FLAGS.finetune)
    if FLAGS.resume is not None:
        logging.info("Resume training from: %s", FLAGS.resume)

    n_train_batches = meta["size"][0] // FLAGS.batch_size
    if meta["size"][0] % FLAGS.batch_size != 0:
        n_train_batches += 1

    n_val_batches = meta["size"][1] // FLAGS.batch_size
    if meta["size"][1] % FLAGS.batch_size != 0:
        n_val_batches += 1
    n_val_batches = int(n_val_batches * FLAGS.val_ratio)

    # Dataset preparation
    train_data = tf.data.TFRecordDataset(
        os.path.join(FLAGS.data_dir, "train.tfrecords"))
    train_data = train_data.map(
        lambda x: parse_example(x, meta["n_views"], meta["shape"]))
    train_data = train_data.shuffle(buffer_size=10000)
    train_data = train_data.batch(FLAGS.batch_size)
    train_data = train_data.repeat()
    train_batch = train_data.make_initializable_iterator()
    train_labels, _, train_views = train_batch.get_next()

    val_data = tf.data.TFRecordDataset(
        os.path.join(FLAGS.data_dir, "validation.tfrecords"))
    val_data = val_data.map(
        lambda x: parse_example(x, meta["n_views"], meta["shape"]))
    val_data = val_data.batch(FLAGS.batch_size)
    val_data = val_data.repeat()
    val_batch = val_data.make_initializable_iterator()
    val_labels, val_files, val_views = val_batch.get_next()

    # Network model
    train_logits, train_vars, restore_vars = model_selector(
        train_views,
        FLAGS.network,
        meta["n_categories"],
        meta["n_views"],
        None,
        meta["data_format"],
        True,
        False,
        FLAGS.finetune)

    val_logits, _, _ = model_selector(
        val_views,
        FLAGS.network,
        meta["n_categories"],
        meta["n_views"],
        None,
        meta["data_format"],
        False,
        True,
        None)

    # TF operators
    train_loss = tf.losses.sparse_softmax_cross_entropy(
        train_labels, train_logits, scope="train_loss")

    val_loss = tf.losses.sparse_softmax_cross_entropy(
        val_labels, val_logits, scope="validation_loss")
    val_loss, val_loss_update = tf.metrics.mean(
        val_loss, name="avg_validation_loss")

    global_step = tf.Variable(0, trainable=False, name="global_step")
    lr = tf.train.exponential_decay(
        learning_rate=FLAGS.learning_rate,
        global_step=global_step,
        decay_steps=FLAGS.decay_steps,
        decay_rate=FLAGS.decay_rate,
        staircase=True)

    if train_vars:
        train_op = tf.train.AdamOptimizer(lr, name="optimizer").minimize(
            train_loss, global_step=global_step, var_list=train_vars)
    else:
        train_op = tf.train.AdamOptimizer(lr, name="optimizer").minimize(
            train_loss, global_step=global_step)

    prediction = tf.argmax(val_logits, 1, name="prediction")

    false_pred_index = tf.where(tf.not_equal(prediction, val_labels))

    accuracy, acc_update = tf.metrics.accuracy(
        val_labels, prediction, name="validation_accuracy")

    init_global = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    train_sum, val_sum = [], []
    train_sum.append(tf.summary.scalar("train_loss", train_loss))
    train_sum.append(tf.summary.scalar("learning_rate", lr))
    val_sum.append(tf.summary.scalar("validation_loss", val_loss))
    val_sum.append(tf.summary.scalar("validation_accuracy", accuracy))
    train_sum = tf.summary.merge(train_sum)
    val_sum = tf.summary.merge(val_sum)

    # Train loop
    with tf.Session() as sess:
        sum_writer = tf.summary.FileWriter(log_dir, sess.graph)
        sess.run(init_global)
        if FLAGS.finetune:
            _, file_ext = os.path.splitext(FLAGS.finetune)
            if file_ext == ".npy":
                restorer = np.load(FLAGS.finetune).item()
                for layer in restore_vars:
                    for weight in restore_vars[layer]:
                        sess.run(restore_vars[layer][weight]
                                 .assign(restorer[layer][weight]))
            else:  # "ckpt"
                restorer = tf.train.Saver(restore_vars)
                restorer.restore(sess, FLAGS.finetune)
        if FLAGS.resume:
            restorer = tf.train.Saver()
            restorer.restore(sess, FLAGS.resume)
        saver = tf.train.Saver()

        if FLAGS.debug and not FLAGS.resume:
            sess.run([train_batch.initializer,
                      val_batch.initializer,
                      init_local])

            # validate inputs
            views, labels = sess.run([train_views, train_labels])
            label = labels[0]
            imgs = views[0]
            if meta["data_format"] == "NCHW":
                imgs = imgs.transpose(0, 2, 3, 1)
            input_sum = tf.summary.image(
                meta["categories"][label]["name"],
                imgs,
                max_outputs=3,
                family="input_views")
            sum_str = sess.run(input_sum)
            sum_writer.add_summary(sum_str)

            # collect training dynamics
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            sess.run([train_op, train_loss],
                     options=run_options,
                     run_metadata=run_metadata)
            sum_writer.add_run_metadata(run_metadata, "training")
            sess.run([val_loss, accuracy, false_pred_index],
                     options=run_options,
                     run_metadata=run_metadata)
            sum_writer.add_run_metadata(run_metadata, "validation")

            sum_writer.flush()

            sess.run(tf.assign(global_step, 0))

        logging.info("================Training Info================")
        logging.info("  training set size: %d", meta["size"][0])
        logging.info("  validation set size: %d, validation ratio: %f",
                     meta["size"][1], FLAGS.val_ratio)
        logging.info(
            "  batch size: %d, training steps: %d, training epochs: %d",
            FLAGS.batch_size,
            FLAGS.train_steps,
            FLAGS.train_steps // n_train_batches)
        logging.info("  lr: %f, decay: %f, decay steps: %d",
                     FLAGS.learning_rate,
                     FLAGS.decay_rate,
                     FLAGS.decay_steps)

        logging.info("================Training Start===============")
        sess.run([train_batch.initializer, val_batch.initializer])
        train_start = time.time()
        while global_step.eval(sess) < FLAGS.train_steps:
            _, loss = sess.run([train_op, train_loss])
            step = global_step.eval(sess)

            if step % FLAGS.log_steps == 0:
                logging.info(
                    "[Step%d/%d]  train loss: %.5f  elapsed time: %.3fs",
                    step,
                    FLAGS.train_steps,
                    loss,
                    time.time() - train_start)
                sum_str = sess.run(train_sum)
                sum_writer.add_summary(sum_str, global_step=step)
                sum_writer.flush()

            if step % FLAGS.val_steps == 0:
                logging.info("[Step%d/%d]  validation triggered",
                             step, FLAGS.train_steps)
                sess.run(init_local)  # reset metrics
                for _ in range(n_val_batches):
                    _, _, preds, fps, files, labels = sess.run([
                        val_loss_update,
                        acc_update,
                        prediction,
                        false_pred_index,
                        val_files,
                        val_labels])
                    for fp in fps:
                        logging.info(
                            "[False Pred]  file: %s  pred: %s  label: %s",
                            str(files[fp[0]][0], "utf-8"),
                            meta["categories"][preds[fp[0]]]["name"],
                            meta["categories"][labels[fp[0]]]["name"])
                avg_loss, avg_acc = sess.run([val_loss, accuracy])
                logging.info("[Validation]  val loss: %.6f  "
                             "val acc: %.6f  elapsed time: %.3fs",
                             avg_loss,
                             avg_acc,
                             time.time() - train_start)
                sum_str = sess.run(val_sum)
                sum_writer.add_summary(sum_str, global_step=step)
                sum_writer.flush()

            if step % FLAGS.save_steps == 0:
                logging.info("[Step%d/%d]  saving triggered",
                             step, FLAGS.train_steps)
                ckpt_file = os.path.join(FLAGS.save_dir, FLAGS.network)
                f = saver.save(sess, ckpt_file, global_step=global_step)
                logging.info("[Saving] saving to %s  elapsed time:%.3fs",
                             f, time.time() - train_start)


def main(argv):
    """Dummy main entry.
    """
    tf.app.run(_main, argv)


if __name__ == "__main__":
    main(sys.argv)
