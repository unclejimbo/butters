"""Script for testing.
"""
import datetime
import json
import logging
import os
import sys
import time

import numpy as np
import tensorflow as tf
import tensorflow.contrib.slim as slim

from common import *

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("data_dir", None,
                           """Directory to tfrecords.""")
tf.app.flags.DEFINE_string("model", None,
                           """Path to the testing model.""")
tf.app.flags.DEFINE_string("network", "vgg_m_1024",
                           """Choose from ["vgg_m_1024"]""")
tf.app.flags.DEFINE_integer("batch_size", 64,
                            """Batch size.""")
tf.app.flags.DEFINE_string("log_dir", "./log/",
                           """Directory to save log and testing summary.""")


def _main(_):
    """Main entry.
    """
    # Logging setup
    if not os.access(FLAGS.log_dir, os.F_OK):
        os.mkdir(FLAGS.log_dir)
    level = logging.INFO
    format = "%(message)s"
    log_dir = "test_" + datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    log_dir = os.path.join(FLAGS.log_dir, log_dir)
    os.mkdir(log_dir)
    log_file = os.path.join(log_dir, "test.log")
    handlers = [logging.FileHandler(filename=log_file, mode="w"),
                logging.StreamHandler()]
    logging.basicConfig(level=level, format=format, handlers=handlers)

    # Meta data
    meta_file = open(os.path.join(FLAGS.data_dir, "meta.json"))
    meta = json.load(meta_file)

    logging.info("==================Overview====================")
    logging.info("Model: " + FLAGS.model)
    logging.info("Dataset directory: " + FLAGS.data_dir)

    n_test_cat = [None] * meta["n_categories"]  # test count per category
    for cat in meta["categories"]:
        n_test_cat[cat["label"]] = cat["size"][2]

    n_test_steps = meta["size"][2] // FLAGS.batch_size
    if meta["size"][2] % FLAGS.batch_size != 0:
        n_test_steps += 1

    # Dataset preparation
    data = tf.data.TFRecordDataset(
        os.path.join(FLAGS.data_dir, "test.tfrecords"))
    data = data.map(
        lambda x: parse_example(x, meta["n_views"], meta["shape"]))
    data = data.batch(FLAGS.batch_size)
    data = data.repeat()
    test_batch = data.make_initializable_iterator()
    test_labels, test_files, test_views = test_batch.get_next()

    # Network model
    logits, _, _ = model_selector(
        test_views,
        FLAGS.network,
        meta["n_categories"],
        meta["n_views"],
        meta["format"],
        False,
        False,
        None)

    # TF operators
    loss = tf.losses.sparse_softmax_cross_entropy(
        test_labels, logits, scope="loss")
    loss, loss_update = tf.metrics.mean(
        loss, name="avg_loss")
    prediction = tf.argmax(logits, 1, name="prediction")
    false_pred_index = tf.where(tf.not_equal(prediction, test_labels))
    accuracy, acc_update = tf.metrics.accuracy(
        test_labels, prediction, name="accuracy")

    init_global = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    # Test loop
    with tf.Session() as sess:
        sess.run([test_batch.initializer, init_global, init_local])

        restorer = tf.train.Saver()
        restorer.restore(sess, FLAGS.model)

        logging.info("===============Testing Start=================")

        # wrong pred count per category
        cat_err_cnt = [0] * meta["n_categories"]

        for step in range(n_test_steps):
            sys.stdout.write("{}/{}\r".format(step, n_test_steps))
            _, _, preds, fps, files, labels = sess.run([
                loss_update,
                acc_update,
                prediction,
                false_pred_index,
                test_files,
                test_labels])

            for fp in fps:
                fp = fp[0]
                cat_err_cnt[labels[fp]] += 1
                logging.info("[False Pred]  file: {}  pred: {}  label: {}"
                             .format(str(files[fp][0], "utf-8"),
                                     meta["categories"][preds[fp]]["name"],
                                     meta["categories"][labels[fp]]["name"]))

        avg_loss, avg_acc = sess.run([loss, accuracy])

        logging.info("==================Summary====================")
        logging.info("Total loss: {:.6f}  Total accuracy: {:.6f}"
                     .format(avg_loss, avg_acc))
        for i, err in enumerate(cat_err_cnt):
            logging.info("Category: {}  Accuracy: {:.6f}".format(
                meta["categories"][i]["name"],
                float(n_test_cat[i] - err) / float(n_test_cat[i])))


def main(argv):
    """Dummy main entry.
    """
    tf.app.run(_main, argv)


if __name__ == "__main__":
    main(sys.argv)
