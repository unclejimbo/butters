"""Inspect the content of a npy file.
"""
import sys
import numpy as np


def inspect_npy(file):
    """Inspect the content of a npy file.
    """
    weights = np.load(file).tolist()
    for layer in weights:
        for var in weights[layer]:
            print("{}/{}: {}".format(layer, var, weights[layer][var].shape))


if __name__ == "__main__":
    inspect_npy(sys.argv[1])
