#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <vector>
#include <experimental/filesystem>

#include <boost/program_options.hpp>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>

#define EUCLID_NO_WARNING
#include <Euclid/Analysis/AABB.h>
#include <Euclid/Analysis/ViewSelection.h>
#include <Euclid/Geometry/MeshHelpers.h>
#include <Euclid/IO/OffIO.h>
#include <Euclid/Math/Vector.h>
#include <Euclid/Render/RayTracer.h>

#include <dlib/clustering.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

namespace fs = std::experimental::filesystem;
namespace po = boost::program_options;

using Kernel = CGAL::Simple_cartesian<float>;
using Point_3 = Kernel::Point_3;
using Mesh = CGAL::Surface_mesh<Point_3>;
using Sample = dlib::matrix<float, 3, 1>;

const constexpr int g_width = 224;
const constexpr int g_ntoken = 4;
const std::vector<float> g_zoom_levels{1.5f, 2.0f, 2.5f};

enum class ProjectionMode
{
    perspective,
    orthogonal
};

enum class ViewMode
{
    aabb,
    diagonal,
    best,
    cat_best,
    m12
};

enum class RenderMode
{
    shaded,
    depth
};

// generate camera
std::unique_ptr<Euclid::Camera> camera(const std::vector<float>& positions,
                                       const std::vector<unsigned>& indices,
                                       int width,
                                       int height,
                                       float scale,
                                       ProjectionMode projection,
                                       ViewMode view)
{
    auto aabb = Euclid::AABB<Kernel>(positions);
    auto diag_len = Euclid::length(aabb.point(0, 0, 0) - aabb.center());

    std::unique_ptr<Euclid::Camera> cam;
    if (projection == ProjectionMode::perspective) {
        Euclid::PerspectiveCamera c;
        c.set_aspect(width, height);
        c.set_fov(60.0f);
        cam = std::make_unique<Euclid::PerspectiveCamera>(c);
    }
    else {
        Euclid::OrthogonalCamera c;
        c.set_extent(diag_len * scale, diag_len * scale);
        cam = std::make_unique<Euclid::OrthogonalCamera>(c);
    }

    auto focus = Euclid::cgal_to_eigen<float>(aabb.center());
    Eigen::Vector3f pos;
    Eigen::Vector3f up(0.0f, 0.0f, 1.0f);

    // use the normal of the largest face of bounding box as view direction
    if (view == ViewMode::aabb) {
        auto a1 = Euclid::area(
            aabb.point(0, 0, 0), aabb.point(0, 1, 0), aabb.point(0, 1, 1));
        auto a2 = Euclid::area(
            aabb.point(0, 0, 0), aabb.point(1, 0, 0), aabb.point(1, 0, 1));
        auto a3 = Euclid::area(
            aabb.point(0, 0, 0), aabb.point(1, 0, 0), aabb.point(1, 1, 0));

        Eigen::Vector3f view_dir;
        if (a1 >= a2 && a1 >= a3) {
            view_dir = Eigen::Vector3f(1.0f, 0.0f, 0.0f);
        }
        else if (a2 >= a1 && a2 >= a3) {
            view_dir = Eigen::Vector3f(0.0f, 1.0f, 0.0f);
        }
        else {
            view_dir = Eigen::Vector3f(0.0f, 0.0f, 1.0f);
            up = Eigen::Vector3f(0.0f, 1.0f, 0.0f);
        }
        pos = focus + view_dir * diag_len * scale;
    }
    // use the diagnoal of the bounding box as view direction
    else if (view == ViewMode::diagonal) {
        auto view_dir =
            Euclid::cgal_to_eigen<float>(aabb.point(1, 1, 1) - aabb.center())
                .normalized();
        pos = focus + view_dir * diag_len * scale;
    }
    // use view selection to choose the best view
    else if (view == ViewMode::best) {
        Mesh mesh;
        Euclid::make_mesh<3>(mesh, positions, indices);
        Mesh view_sphere;
        std::vector<float> view_scores;
        Euclid::proxy_view_selection(
            mesh, view_sphere, view_scores, 4, 0.8f, scale, up);

        auto smax = std::max_element(view_scores.begin(), view_scores.end());
        auto smaxid =
            static_cast<Mesh::Vertex_index>(smax - view_scores.begin());
        pos = Euclid::cgal_to_eigen<float>(view_sphere.point(smaxid));
    }

    cam->lookat(pos, focus, up);
    return cam;
}

// generate camera given view direction
std::unique_ptr<Euclid::Camera> camera(std::vector<float>& positions,
                                       int width,
                                       int height,
                                       float scale,
                                       ProjectionMode projection,
                                       const Eigen::Vector3f& view_dir)
{
    if (positions.size() % 3 != 0) { // pop padding
        positions.pop_back();
    }

    auto aabb = Euclid::AABB<Kernel>(positions);
    auto diag_len = Euclid::length(aabb.point(0, 0, 0) - aabb.center());

    std::unique_ptr<Euclid::Camera> cam;
    if (projection == ProjectionMode::perspective) {
        Euclid::PerspectiveCamera c;
        c.set_aspect(width, height);
        c.set_fov(60.0f);
        cam = std::make_unique<Euclid::PerspectiveCamera>(c);
    }
    else {
        Euclid::OrthogonalCamera c;
        c.set_extent(diag_len * scale, diag_len * scale);
        cam = std::make_unique<Euclid::OrthogonalCamera>(c);
    }

    auto focus = Euclid::cgal_to_eigen<float>(aabb.center());
    Eigen::Vector3f pos = focus + view_dir * diag_len * scale;
    Eigen::Vector3f up(0.0f, 0.0f, 1.0f);
    cam->lookat(pos, focus, up);

    return cam;
}

std::vector<unsigned char> render(std::vector<float>& positions,
                                  const std::vector<unsigned>& indices,
                                  const std::string& img_file,
                                  const Euclid::Camera& camera,
                                  int width,
                                  int height,
                                  RenderMode mode = RenderMode::shaded)
{
    if (positions.size() % 3 != 1) { // padding for embree
        positions.push_back(0.0f);
    }

    Euclid::RayTracer raytracer;
    raytracer.attach_geometry_shared(positions, indices);
    std::vector<unsigned char> pixels(width * height * 3);
    if (mode == RenderMode::shaded) {
        raytracer.render_shaded(pixels.data(), camera, width, height, 8);
    }
    else { // depth
        std::vector<float> depth(width * height);
        raytracer.render_depth(depth.data(), camera, width, height, false);
        auto dmax = std::max_element(depth.begin(), depth.end());
        for (size_t i = 0; i < depth.size(); ++i) {
            auto value = static_cast<unsigned char>(depth[i] / *dmax * 255);
            pixels[3 * i + 0] = value;
            pixels[3 * i + 1] = value;
            pixels[3 * i + 2] = value;
        }
    }
    return pixels;
}

void run(const std::vector<std::string>& fin,
         const std::vector<std::string>& fout,
         ProjectionMode proj,
         ViewMode view,
         RenderMode shading)
{
#pragma omp parallel for
    for (size_t i = 0; i < fin.size(); ++i) {
        // read off
        std::vector<float> positions;
        std::vector<unsigned> indices;
        Euclid::read_off<3>(fin[i], positions, indices);

        // render and write png
        if (view == ViewMode::m12) { // multi-views
            std::vector<Eigen::Vector3f> view_dirs;
            for (int j = 0; j < 12; ++j) {
                auto x = std::cos(M_PI / 6 * j);
                auto y = std::sin(M_PI / 6 * j);
                auto z = std::cos(M_PI / 6);
                view_dirs.emplace_back(x, y, z);
                view_dirs[j].normalize();
            }

            for (size_t j = 0; j < view_dirs.size(); ++j) {
                auto cam = camera(
                    positions, g_width, g_width, 2.0f, proj, view_dirs[j]);
                auto fimg = fout[i];
                fimg.append("_").append(std::to_string(j)).append(".png");
                auto pixels = render(
                    positions, indices, fimg, *cam, g_width, g_width, shading);

                std::cout << "writing to: " << fimg << std::endl;
                stbi_write_png(fimg.c_str(),
                               g_width,
                               g_width,
                               3,
                               pixels.data(),
                               g_width * 3);
            }
        }
        else { // single view
            auto cam =
                camera(positions, indices, g_width, g_width, 2.0f, proj, view);
            auto fimg = fout[i];
            fimg.append(".png");
            auto pixels = render(
                positions, indices, fimg, *cam, g_width, g_width, shading);

            std::cout << "writing to: " << fimg << std::endl;
            stbi_write_png(
                fimg.c_str(), g_width, g_width, 3, pixels.data(), g_width * 3);
        }
    }
}

int main(int argc, char* argv[])
{
    po::options_description desc("Allowed commands");
    desc.add_options()("help,h", "produce help message")(
        "input_file,i",
        po::value<std::string>(),
        "input path to a single mesh")("output_file,o",
                                       po::value<std::string>(),
                                       "output path to a single mesh")(
        "input_directory,I",
        po::value<std::string>(),
        "input path to a directory of meshes")("output_directory,O",
                                               po::value<std::string>(),
                                               "output path to a directory")(
        "shading,s", po::value<std::string>(), "shading mode, shaded or depth")(
        "projection,p",
        po::value<std::string>(),
        "projection mode, perspective or orthogonal")(
        "view,v",
        po::value<std::string>(),
        "view mode, aabb, diagonal, best, cat_best, or m12");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }

    auto projection = ProjectionMode::orthogonal;
    if (vm.count("projection")) {
        if (vm["projection"].as<std::string>() == "perpective") {
            projection = ProjectionMode::perspective;
        }
        else if (vm["projection"].as<std::string>() == "orthogonal") {
            projection = ProjectionMode::orthogonal;
        }
        else {
            std::cout << desc << std::endl;
            return 1;
        }
    }

    auto view = ViewMode::diagonal;
    if (vm.count("view")) {
        if (vm["view"].as<std::string>() == "aabb") { view = ViewMode::aabb; }
        else if (vm["view"].as<std::string>() == "diagonal") {
            view = ViewMode::diagonal;
        }
        else if (vm["view"].as<std::string>() == "best") {
            view = ViewMode::best;
        }
        else if (vm["view"].as<std::string>() == "cat_best") {
            view = ViewMode::cat_best;
        }
        else if (vm["view"].as<std::string>() == "m12") {
            view = ViewMode::m12;
        }
        else {
            std::cout << desc << std::endl;
            return 1;
        }
    }

    auto shading = RenderMode::shaded;
    if (vm.count("shading")) {
        if (vm["shading"].as<std::string>() == "shaded") {
            shading = RenderMode::shaded;
        }
        else if (vm["shading"].as<std::string>() == "depth") {
            shading = RenderMode::depth;
        }
        else {
            std::cout << desc << std::endl;
            return 1;
        }
    }

    if (vm.count("input_file")) { // single mesh
        std::string file_in = vm["input_file"].as<std::string>();
        std::string file_out;
        if (vm.count("output_file")) {
            file_out = vm["output_file"].as<std::string>();
        }
        else {
            file_out = "out.png";
        }

        std::vector<float> positions;
        std::vector<unsigned> indices;
        Euclid::read_off<3>(file_in, positions, indices);
        auto cam = camera(
            positions, indices, g_width, g_width, 2.0f, projection, view);
        render(positions, indices, file_out, *cam, g_width, g_width, shading);
        return 0;
    }
    else if (vm.count("input_directory")) { // batch
        std::string directory_in = vm["input_directory"].as<std::string>();
        std::string directory_out;
        if (vm.count("output_directory")) {
            directory_out = vm["output_directory"].as<std::string>();
            if (!fs::exists(fs::path(directory_out))) {
                fs::create_directory(directory_out);
            }
        }
        else {
            directory_out = directory_in;
        }

        // collect file names
        std::vector<std::string> fin;
        std::vector<std::string> fout;
        for (const auto& p : fs::directory_iterator(directory_in)) {
            auto train_path = p.path();
            train_path.append("train");
            auto test_path = p.path();
            test_path.append("test");
            auto category = p.path().filename();

            auto out_path = fs::path(directory_out).append(category.c_str());
            fs::create_directory(out_path);

            auto out_train_path = out_path;
            out_train_path.append("train");
            fs::create_directory(out_train_path);

            auto out_test_path = out_path;
            out_test_path.append("test");
            fs::create_directory(out_test_path);

            // // evaluate the best view for a category of models
            // Eigen::Vector3f cat_best_view;
            // if (view == ViewMode::cat_best) {
            //     std::vector<Sample> samples;
            //     for (const auto& f : fs::directory_iterator(train_path)) {
            //         if (f.path().extension().string() == ".off") {
            //             std::vector<float> positions;
            //             std::vector<unsigned> indices;
            //             Euclid::read_off<3>(
            //                 f.path().string(), positions, indices);

            //             auto aabb = Euclid::AABB<Kernel>(positions);

            //             Mesh mesh;
            //             Euclid::make_mesh<3>(mesh, positions, indices);

            //             Mesh view_sphere;
            //             std::vector<float> view_scores;
            //             Eigen::Vector3f up(0.0f, 0.0f, 1.0f);
            //             Euclid::proxy_view_selection(
            //                 mesh, view_sphere, view_scores, 4, 0.8f, 2.0f,
            //                 up);

            //             auto smax = std::max_element(view_scores.begin(),
            //                                          view_scores.end());
            //             auto smaxid = static_cast<Mesh::Vertex_index>(
            //                 smax - view_scores.begin());
            //             auto p = view_sphere.point(smaxid);
            //             // only record upper semi-sphere
            //             if (p.z() > aabb.center().z()) {
            //                 auto v = Euclid::normalized(p - aabb.center());
            //                 Sample s;
            //                 s(0) = v.x();
            //                 s(1) = v.y();
            //                 s(2) = v.z();
            //                 samples.push_back(s);
            //             }
            //         }
            //     }

            //     // using angular kmeans, suitable for clustering on sphere
            //     // pick 4 centers, that's a heuristic
            //     std::vector<Sample> centers;
            //     dlib::pick_initial_centers(4, centers, samples);
            //     dlib::find_clusters_using_angular_kmeans(samples, centers);

            //     // find best center
            //     std::vector<int> cluster_scores(centers.size(), 0);
            //     for (size_t i = 0; i < samples.size(); ++i) {
            //         auto idx = dlib::nearest_center(centers, samples[i]);
            //         ++cluster_scores[idx];
            //     }
            //     auto cluster_max = std::max_element(cluster_scores.begin(),
            //                                         cluster_scores.end());
            //     auto best_cluster =
            //         static_cast<size_t>(cluster_max -
            //         cluster_scores.begin());
            //     cat_best_view(0) = centers[best_cluster](0);
            //     cat_best_view(1) = centers[best_cluster](1);
            //     cat_best_view(2) = centers[best_cluster](2);
            // }

            for (const auto& f : fs::directory_iterator(train_path)) {
                if (f.path().extension().string() == ".off") {
                    auto img_path = out_train_path;
                    img_path.append(f.path().stem().c_str());
                    fin.push_back(f.path().string());
                    fout.push_back(img_path.string());
                }
            }

            for (const auto& f : fs::directory_iterator(test_path)) {
                if (f.path().extension().string() == ".off") {
                    auto img_path = out_test_path;
                    img_path.append(f.path().stem().c_str());
                    fin.push_back(f.path().string());
                    fout.push_back(img_path.string());
                }
            }

            break;
        }

        // read and render
        auto t0 = std::chrono::high_resolution_clock::now();
        run(fin, fout, projection, view, shading);
        auto t1 = std::chrono::high_resolution_clock::now();
        auto tdiff =
            std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        std::cout << "Time: " << tdiff.count() << std::endl;

        return 0;
    }
    else {
        std::cout << desc << std::endl;
        return 1;
    }
}
