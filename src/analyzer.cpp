#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <experimental/filesystem>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <boost/program_options.hpp>
#include <matplotlibcpp.h>

namespace fs = std::experimental::filesystem;
namespace po = boost::program_options;
namespace plt = matplotlibcpp;

std::vector<int> histogram(const cv::Mat img)
{
    const int channel[]{0};
    cv::Mat mask;
    const int hist_size[]{256};
    float color_range[]{0, 256};
    const float* hist_range[]{color_range};
    cv::Mat hist;
    cv::calcHist(&img, 1, channel, mask, hist, 1, hist_size, hist_range);

    std::vector<int> values(256);
    hist.copyTo(values);
    return values;
}

int main(int argc, char* argv[])
{
    po::options_description desc("Allowed commands");
    desc.add_options()("help,h", "produce help message")(
        "input_file,i", po::value<std::string>(), "path to input image")(
        "input_directory,I",
        po::value<std::string>(),
        "path to input directory")("output_directory,O",
                                   po::value<std::string>(),
                                   "path to output directory");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("input_file")) {
        auto imgfile = vm["input_file"].as<std::string>();
        std::string grayfile;
        std::string laplacianfile;
        if (vm.count("output_directory")) {
            grayfile = vm["output_directory"].as<std::string>();
            grayfile.append("/gray.png");
            laplacianfile = vm["output_directory"].as<std::string>();
            laplacianfile.append("/laplacian.png");
        }
        else {
            grayfile = "./gray.png";
            laplacianfile = "./laplacian.png";
        }
        cv::Mat img = cv::imread(imgfile, cv::IMREAD_GRAYSCALE);
        std::vector<int> x(256);
        std::iota(x.begin(), x.end(), 0);

        // image histogram
        auto grayhist = histogram(img);
        grayhist[0] = 0;
        plt::plot(x, grayhist);
        plt::save(grayfile);

        // laplacian histogram
        cv::Mat laplacian_img;
        cv::Laplacian(img, laplacian_img, CV_8U);
        auto laplacianhist = histogram(laplacian_img);
        laplacianhist[0] = 0;
        plt::clf();
        plt::plot(x, laplacianhist);
        plt::save(laplacianfile);

        return 0;
    }
    else if (vm.count("input_directory")) {
        std::string directory_in = vm["input_directory"].as<std::string>();
        std::string directory_out;
        if (vm.count("output_directory")) {
            directory_out = vm["output_directory"].as<std::string>();
            if (!fs::exists(fs::path(directory_out))) {
                fs::create_directory(directory_out);
            }
        }
        else {
            directory_out = directory_in;
        }

        std::vector<int> x(256);
        std::iota(x.begin(), x.end(), 0);
        std::vector<double> gray_all_avg(256, 0.0);
        std::vector<double> laplacian_all_avg(256, 0.0);
        for (const auto& p : fs::directory_iterator(directory_in)) {
            auto test_path = p.path();
            test_path.append("test");
            auto category = p.path().filename();
            std::cout << category << std::endl;

            std::vector<double> gray_cat_avg(256, 0.0);
            std::vector<double> laplacian_cat_avg(256, 0.0);
            int filecnt = 0;
            for (const auto& f : fs::directory_iterator(test_path)) {
                cv::Mat img =
                    cv::imread(f.path().string(), cv::IMREAD_GRAYSCALE);

                auto grayhist = histogram(img);
                for (size_t i = 1; i < 256; ++i) {
                    gray_cat_avg[i] += grayhist[i];
                }

                cv::Mat laplacian_img;
                cv::Laplacian(img, laplacian_img, CV_8U);
                auto laplacianhist = histogram(laplacian_img);
                for (size_t i = 1; i < 256; ++i) {
                    laplacian_cat_avg[i] += laplacianhist[i];
                }

                ++filecnt;
            }

            for (size_t i = 1; i < 256; ++i) {
                gray_cat_avg[i] /= filecnt;
                gray_all_avg[i] += gray_cat_avg[i] / 40;
                laplacian_cat_avg[i] /= filecnt;
                laplacian_all_avg[i] += laplacian_cat_avg[i] / 40;
            }

            plt::clf();
            plt::subplot(2, 1, 1);
            plt::plot(x, gray_cat_avg);
            plt::subplot(2, 1, 2);
            plt::plot(x, laplacian_cat_avg);
            auto histfile = directory_out;
            histfile.append("/").append(category).append(".png");
            plt::save(histfile);
        }
        // auto histall = directory_out;
        // histall.append("/all.png");
        // plt::save(histall);
        auto histavg = directory_out;
        histavg.append("/avg.png");
        plt::clf();
        plt::subplot(2, 1, 1);
        plt::plot(x, gray_all_avg);
        plt::subplot(2, 1, 2);
        plt::plot(x, laplacian_all_avg);
        plt::save(histavg);
        return 0;
    }
    else {
        std::cout << desc << std::endl;
        return 1;
    }
}
