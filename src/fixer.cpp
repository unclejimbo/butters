#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <experimental/filesystem>

#include <boost/program_options.hpp>
#include <Euclid/IO/OffIO.h>
#include <Euclid/IO/InputFixer.h>
#include <ThreadPool.h>

namespace fs = std::experimental::filesystem;
namespace po = boost::program_options;

void fix_model(const std::string& in_file, const std::string& out_file)
{
    try {
        std::vector<float> positions;
        std::vector<unsigned> indices;
        Euclid::read_off<3>(in_file, positions, indices);

        std::cout << "fixing...     " << in_file << std::endl;
        // TODO:
        // potential bug in Euclid
        // auto n1 = Euclid::remove_duplicate_vertices<3>(positions, indices);
        auto n2 = Euclid::remove_duplicate_faces<3>(indices);
        auto n3 = Euclid::remove_degenerate_faces<3>(positions, indices);
        auto n4 = Euclid::remove_unreferenced_vertices<3>(positions, indices);
        Euclid::write_off<3>(out_file, positions, indices);
        std::cout << "writing to... " << out_file << std::endl;
    }
    catch (const std::exception& e) {
        std::cout << e.what() << std::endl;
    }
}

int main(int argc, char* argv[])
{
    po::options_description desc("Allowed commands");
    desc.add_options()("help,h", "produce help message")(
        "input_file,i",
        po::value<std::string>(),
        "input path to a single mesh")("output_file,o",
                                       po::value<std::string>(),
                                       "output path to a single mesh")(
        "input_directory,I",
        po::value<std::string>(),
        "input path to a directory of meshes")("output_directory,O",
                                               po::value<std::string>(),
                                               "output path to a directory");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }
    else if (vm.count("input_file")) {
        std::string file_in = vm["input_file"].as<std::string>();
        std::string file_out;
        if (vm.count("output_file")) {
            file_out = vm["output_file"].as<std::string>();
        }
        else {
            file_out = file_in;
        }
        fix_model(file_in, file_out);
        return 0;
    }
    else if (vm.count("input_directory")) {
        std::string directory_in = vm["input_directory"].as<std::string>();
        std::string directory_out;
        if (vm.count("output_directory")) {
            directory_out = vm["output_directory"].as<std::string>();
        }
        else {
            directory_out = directory_in;
        }

        ThreadPool pool(8);
        for (const auto& p : fs::directory_iterator(directory_in)) {
            auto train_path = p.path();
            train_path.append("train");
            auto test_path = p.path();
            test_path.append("test");
            auto category = p.path().filename();

            auto out_path = fs::path(directory_out).append(category.c_str());
            fs::create_directory(out_path);

            auto out_train_path = out_path;
            out_train_path.append("train");
            fs::create_directory(out_train_path);

            auto out_test_path = out_path;
            out_test_path.append("test");
            fs::create_directory(out_test_path);

            for (const auto& f : fs::directory_iterator(train_path)) {
                if (f.path().extension().string() == ".off") {
                    auto file_name = f.path().filename();
                    auto out_file_path = out_train_path;
                    out_file_path.append(file_name.c_str());

                    pool.enqueue(
                        fix_model, f.path().string(), out_file_path.string());
                }
            }

            for (const auto& f : fs::directory_iterator(test_path)) {
                if (f.path().extension().string() == ".off") {
                    auto file_name = f.path().filename();
                    auto out_file_path = out_test_path;
                    out_file_path.append(file_name.c_str());

                    pool.enqueue(
                        fix_model, f.path().string(), out_file_path.string());
                }
            }
        }

        return 0;
    }
    else {
        std::cout << desc << std::endl;
        return 1;
    }
}
